#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"
#include <list>
#include "cinder/System.h"
#include "cinder/Rand.h"
#include "cinder/Log.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class MultiWindowsMultiTouchscreenApp : public App {
  public:
	  void setup();
	  void createNewWindow();

	  void mouseDrag(MouseEvent event);
	  void touchesMoved(TouchEvent event) override;

	  void keyDown(KeyEvent event);
	  void draw();
};

void prepareSettings(MultiWindowsMultiTouchscreenApp::Settings *settings)
{
	// By default, multi-touch is disabled on desktop and enabled on mobile platforms.
	// You enable multi-touch from the SettingsFn that fires before the app is constructed.
	settings->setMultiTouchEnabled(true);

	// On mobile, if you disable multitouch then touch events will arrive via mouseDown(), mouseDrag(), etc.
	//	settings->setMultiTouchEnabled( false );
}

// The window-specific data for each window
class WindowData {
public:
	WindowData()
		: mColor(Color(CM_HSV, randFloat(), 0.8f, 0.8f)) // a random color
	{}

	Color			mColor;
	list<vec2>		mPoints; // the points drawn into this window
};

void MultiWindowsMultiTouchscreenApp::setup()
{
	// for the default window we need to provide an instance of WindowData
	getWindow()->setUserData(new WindowData);

	createNewWindow();
}

void MultiWindowsMultiTouchscreenApp::createNewWindow()
{
	app::WindowRef newWindow = createWindow(Window::Format().size(400, 400));
	newWindow->setUserData(new WindowData);
	
	// for demonstration purposes, we'll connect a lambda unique to this window which fires on close
	int uniqueId = getNumWindows();
	newWindow->getSignalClose().connect(
		[uniqueId, this] { this->console() << "You closed window #" << uniqueId << std::endl; }
	);
}

void MultiWindowsMultiTouchscreenApp::mouseDrag(MouseEvent event)
{
	//WindowData *data = getWindow()->getUserData<WindowData>();

	// add this point to the list
	//data->mPoints.push_back(event.getPos());
}

void MultiWindowsMultiTouchscreenApp::touchesMoved(TouchEvent event)
{
	WindowData *data = getWindow()->getUserData<WindowData>();

	// add this point to the list
	for (const auto &touch : event.getTouches()) {
		data->mPoints.push_back(touch.getPos());
	}
}


void MultiWindowsMultiTouchscreenApp::keyDown(KeyEvent event)
{
	if (event.getChar() == 'f')
		setFullScreen(!isFullScreen());
	else if (event.getChar() == 'w')
		createNewWindow();
}

void MultiWindowsMultiTouchscreenApp::draw()
{
	gl::clear(Color(0.1f, 0.1f, 0.15f));

	WindowData *data = getWindow()->getUserData<WindowData>();

	gl::color(data->mColor);
	gl::begin(GL_LINE_STRIP);
	for (auto pointIter = data->mPoints.begin(); pointIter != data->mPoints.end(); ++pointIter) {
		gl::vertex(*pointIter);
	}
	gl::end();
}

CINDER_APP(MultiWindowsMultiTouchscreenApp, RendererGl, prepareSettings)
